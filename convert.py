#!/usr/bin/python3

import os
import json

START_INDEX=1
END_INDEX=3

f = open('config_dashcam.json', 'r')
config_dashcam = json.load(f)
END_INDEX = config_dashcam['file']['end']
START_INDEX = config_dashcam['file']['start']

print("Starting conversion")
if START_INDEX > END_INDEX:
    print("Error, check index values")
else:
    if not os.path.exists("/home/pi/output/"):
        os.makedirs("/home/pi/output/")
        print("Created output folder")

    i = START_INDEX
    while (i<=END_INDEX):
        input_file = "/home/pi/videos/video%05d.h264" % i
        output_file = "/home/pi/output/video%05d.mp4" % i
        i = i +1

        if os.path.exists(input_file):
            print("Converting: " + input_file)
            conversion_command = "MP4Box -add " + input_file + " " + output_file
            print(conversion_command)
            os.system(conversion_command)
    print("Conversion complete")
