#!/usr/bin/python3

import picamera
import os
import psutil
import serial
import pynmea2
import time
import json
import itertools
import RPi.GPIO as GPIO
from picamera import Color

folder_root = "/home/pi/"
videos_folder = "videos/"
port = "/dev/serial1"
file_number = 0

SPACE_LIMIT = 80
MAX_FILES = 99999
DURATION = 60
DELETE_FILES = 100

serialPort = serial.Serial(port, baudrate = 9600, timeout = 0.5)

def clear_space():
    print("we're close to full, clearing space")
    files_deleted = 0
    if os.path.isfile('config_dashcam.json'):
        f = open('config_dashcam.json', 'r')
        config_dashcam = json.load(f)
        files_deleted = config_dashcam['file']['start']

    threshold = DELETE_FILES + files_deleted
    while files_deleted < MAX_FILES:
        del_file_path = folder_root + videos_folder + "video%05d.h264" % files_deleted
        if os.path.exists(del_file_path):
            print ("deleting: " + del_file_path)
            os.remove(del_file_path)
            files_deleted = files_deleted + 1

            if files_deleted >= threshold:
                break;

    config_dashcam['file']['start'] = threshold
    with open('config_dashcam.json', 'w') as f:
        json.dump(config_dashcam, f)
    print("done clearing")
           
    

def check_space():
    if psutil.disk_usage(".").percent > SPACE_LIMIT:
        print("clearing space")
        clear_space()
    else:
        print("you're fine on space")

if os.path.isfile('config_dashcam.json'):
    f = open('config_dashcam.json', 'r')
    config_dashcam = json.load(f)
    file_number = config_dashcam['file']['end']
    print('Obtained: ')
    print(file_number)

else:
    print ('File not found. Creating new with 0...')
    file_number = 0
    config_dashcam = {}
    config_dashcam['file'] = { 'start' : file_number, 'end': file_number }
    with open('config_dashcam.json', 'w') as f:
    	json.dump(config_dashcam, f)
    print('Save complete')

with picamera.PiCamera() as camera:
    camera.resolution = (1920, 1080)
    camera.framerate = 25

    while file_number < MAX_FILES:
        file_number = file_number + 1
        file_name = folder_root + videos_folder + "video%05d.h264" % file_number
        
        print('Recording to %s' % file_name)
        timeout = time.time() + DURATION
        camera.start_recording(file_name, quality=20)
        while (time.time() < timeout):
            gps_str = serialPort.readline()

            if gps_str.find('GGA'.encode()) > 0:
                msg = pynmea2.parse(gps_str)
                camera.annotate_background = Color('black')
                camera.annotate_Text = "TME: %s LAT: %s %s LON: %s %s ALT: %s %s SAT: %s CPU: %s" %(msg.timestamp, msg.lat, mst.lat_dir, msg.lon, mst.lon_dir, msg.altitude, msg.altitude_units, msg.num_sats, psutil.cpu_percent())


            time.sleep(0.02)
        camera.stop_recording()
        config_dashcam['file']['end'] = file_number
        with open('config_dashcam.json', 'w') as f:
            json.dump(config_dashcam, f)
        check_space()

