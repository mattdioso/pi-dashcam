#!/usr/bin/python3
import psutil

SPACE_LIMIT = 80

if psutil.disk_usage(".").percent > SPACE_LIMIT:
    print("OVER LIMIT")
else:
    print("you're fine")
